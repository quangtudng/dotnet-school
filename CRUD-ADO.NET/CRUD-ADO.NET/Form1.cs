﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUD_ADO.NET
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            FillComboBox();
        }
        private void FillComboBox()
        {
            DataTable dt = StudentManagement.Instance.Get_Data("SELECT * FROM Class");
            classComboBox2.Items.Add("All");
            foreach(DataRow row in dt.Rows)
            {
                classComboBox1.Items.Add(new ComboBoxItem() {
                    Text = row["Name"].ToString(),
                    Value = row["ClassID"].ToString(),
                });
                classComboBox2.Items.Add(new ComboBoxItem()
                {
                    Text = row["Name"].ToString(),
                    Value = row["ClassID"].ToString(),
                });
            }
        }
        private void showUser(string classID)
        {
            if (classID == "All")
            {
                dataGridView.DataSource = StudentManagement.Instance.Get_Data("SELECT * FROM SinhVien");
            }
            else
            {
                dataGridView.DataSource = StudentManagement.Instance.Get_Data("SELECT * FROM SinhVien WHERE ClassID=" + classID);
            }
        }
        private void addButton_Click(object sender, EventArgs e)
        {
            string studentID = IDTextBox.Text.ToString();
            string name = nameTextBox.Text.ToString();
            string classID = ((ComboBoxItem)classComboBox1.SelectedItem).Value;
            string birthday = birthdayPicker.Value.Date.ToShortDateString();
            int gender = -1;
            if(maleButton.Checked)
            {
                gender = 1;
            } 
            else if(femaleButton.Checked)
            {
                gender = 0;
            }
            if(string.IsNullOrEmpty(studentID) || string.IsNullOrEmpty(name) || string.IsNullOrEmpty(classID) || string.IsNullOrEmpty(birthday) || gender == -1)
            {
                MessageBox.Show("Please fill in all information");
            }
            else
            {
                string query = "INSERT INTO SinhVien(StudentID,Name,ClassID,Birthday,Gender) VALUES " +
                    "('" +
                        studentID + "','" +
                        name + "','" +
                        classID + "','" +
                        birthday + "','" +
                        gender +
                    "')";
                if (!StudentManagement.Instance.Change_Data(query))
                {
                    MessageBox.Show("An error has occured during adding user");
                }
                else
                {
                    MessageBox.Show("Record added successfully");
                    showUser("All");
                }
            }
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            int gender = 1;
            if(maleButton.Checked)
            {
                gender = 1;
            } 
            if(femaleButton.Checked)
            {
                gender = 0;
            }
            string query = "UPDATE SinhVien SET Name='"
                + nameTextBox.Text +"',classID='"
                + ((ComboBoxItem)classComboBox1.SelectedItem).Value + "',Birthday='"
                + birthdayPicker.Value.Date.ToShortDateString() + "',Gender="
                + gender + " WHERE StudentID='"
                + IDTextBox.Text + "'";
            if(StudentManagement.Instance.Change_Data(query))
            {
                MessageBox.Show("Record changed");
                showUser("All");
            } 
            else
            {
                MessageBox.Show("Failed to change record");
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (StudentManagement.Instance.Change_Data("DELETE FROM SinhVien WHERE StudentID='" + IDTextBox.Text + "'"))
            {
                MessageBox.Show("Deleted Record");
                showUser("All");
                
            } 
            else
            {
                MessageBox.Show("Failed to delete");
            }
        }

        private void sortButton_Click(object sender, EventArgs e)
        {
            if(sortByComboBox.SelectedItem.ToString() == "MSSV")
            {
                dataGridView.Sort(dataGridView.Columns["StudentID"], ListSortDirection.Ascending);
            }
            if (sortByComboBox.SelectedItem.ToString() == "Name")
            {
                dataGridView.Sort(dataGridView.Columns["Name"], ListSortDirection.Ascending);
            }
            if (sortByComboBox.SelectedItem.ToString() == "Birthday")
            {
                dataGridView.Sort(dataGridView.Columns["Birthday"], ListSortDirection.Ascending);
            }
            if (sortByComboBox.SelectedItem.ToString() == "Gender")
            {
                dataGridView.Sort(dataGridView.Columns["Gender"], ListSortDirection.Ascending);
            }
            if (sortByComboBox.SelectedItem.ToString() == "Class")
            {
                dataGridView.Sort(dataGridView.Columns["ClassID"], ListSortDirection.Ascending);
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            DataTable dt = StudentManagement.Instance.Get_Data("SELECT * FROM SinhVien WHERE Name LIKE '" + searchTextBox.Text + "%'");
            dataGridView.DataSource = dt;
        }

        private void classComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (classComboBox2.SelectedItem.ToString() == "All")
            {
                showUser("All");
            } 
            else
            {
                string classID = ((ComboBoxItem)classComboBox2.SelectedItem).Value;
                showUser(classID);
            }
        }

        private void dataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if(dataGridView.SelectedRows.Count < 2)
            {
                IDTextBox.Text = dataGridView.SelectedRows[0].Cells["StudentID"].Value.ToString();
                nameTextBox.Text = dataGridView.SelectedRows[0].Cells["Name"].Value.ToString();
                birthdayPicker.Value = (DateTime)dataGridView.SelectedRows[0].Cells["Birthday"].Value;
                if(Convert.ToBoolean(dataGridView.SelectedRows[0].Cells["Gender"].Value))
                {
                    maleButton.Checked = true;
                } else
                {
                    femaleButton.Checked = true;
                }
                for(int i=0;i< classComboBox1.Items.Count; i++)
                {
                    if(((ComboBoxItem)classComboBox1.Items[i]).Value.ToString() == dataGridView.SelectedRows[0].Cells["ClassID"].Value.ToString())
                    {
                        classComboBox1.SelectedIndex = i;
                    }
                }
            }
        }
    }
}
