﻿namespace CRUD_ADO.NET
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGroupBox = new System.Windows.Forms.GroupBox();
            this.classComboBox1 = new System.Windows.Forms.ComboBox();
            this.genderGroupBox = new System.Windows.Forms.GroupBox();
            this.femaleButton = new System.Windows.Forms.RadioButton();
            this.maleButton = new System.Windows.Forms.RadioButton();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.IDTextBox = new System.Windows.Forms.TextBox();
            this.birthdayPicker = new System.Windows.Forms.DateTimePicker();
            this.birthdayLabel = new System.Windows.Forms.Label();
            this.classLabel1 = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.IDLabel = new System.Windows.Forms.Label();
            this.actionGroupBox = new System.Windows.Forms.GroupBox();
            this.sortByComboBox = new System.Windows.Forms.ComboBox();
            this.sortButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.editButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.searchButton = new System.Windows.Forms.Button();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.searchLabel = new System.Windows.Forms.Label();
            this.classComboBox2 = new System.Windows.Forms.ComboBox();
            this.classLabel2 = new System.Windows.Forms.Label();
            this.dataGroupBox.SuspendLayout();
            this.genderGroupBox.SuspendLayout();
            this.actionGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGroupBox
            // 
            this.dataGroupBox.Controls.Add(this.classComboBox1);
            this.dataGroupBox.Controls.Add(this.genderGroupBox);
            this.dataGroupBox.Controls.Add(this.nameTextBox);
            this.dataGroupBox.Controls.Add(this.IDTextBox);
            this.dataGroupBox.Controls.Add(this.birthdayPicker);
            this.dataGroupBox.Controls.Add(this.birthdayLabel);
            this.dataGroupBox.Controls.Add(this.classLabel1);
            this.dataGroupBox.Controls.Add(this.nameLabel);
            this.dataGroupBox.Controls.Add(this.IDLabel);
            this.dataGroupBox.Location = new System.Drawing.Point(12, 12);
            this.dataGroupBox.Name = "dataGroupBox";
            this.dataGroupBox.Size = new System.Drawing.Size(573, 151);
            this.dataGroupBox.TabIndex = 1;
            this.dataGroupBox.TabStop = false;
            this.dataGroupBox.Text = "Thông tin sinh viên";
            // 
            // classComboBox1
            // 
            this.classComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.classComboBox1.FormattingEnabled = true;
            this.classComboBox1.Location = new System.Drawing.Point(88, 106);
            this.classComboBox1.Name = "classComboBox1";
            this.classComboBox1.Size = new System.Drawing.Size(100, 21);
            this.classComboBox1.TabIndex = 9;
            // 
            // genderGroupBox
            // 
            this.genderGroupBox.Controls.Add(this.femaleButton);
            this.genderGroupBox.Controls.Add(this.maleButton);
            this.genderGroupBox.Location = new System.Drawing.Point(327, 76);
            this.genderGroupBox.Name = "genderGroupBox";
            this.genderGroupBox.Size = new System.Drawing.Size(200, 57);
            this.genderGroupBox.TabIndex = 8;
            this.genderGroupBox.TabStop = false;
            this.genderGroupBox.Text = "Gender";
            // 
            // femaleButton
            // 
            this.femaleButton.AutoSize = true;
            this.femaleButton.Location = new System.Drawing.Point(105, 19);
            this.femaleButton.Name = "femaleButton";
            this.femaleButton.Size = new System.Drawing.Size(67, 19);
            this.femaleButton.TabIndex = 1;
            this.femaleButton.TabStop = true;
            this.femaleButton.Text = "Female";
            this.femaleButton.UseVisualStyleBackColor = true;
            // 
            // maleButton
            // 
            this.maleButton.AutoSize = true;
            this.maleButton.Location = new System.Drawing.Point(15, 19);
            this.maleButton.Name = "maleButton";
            this.maleButton.Size = new System.Drawing.Size(53, 19);
            this.maleButton.TabIndex = 0;
            this.maleButton.TabStop = true;
            this.maleButton.Text = "Male";
            this.maleButton.UseVisualStyleBackColor = true;
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(88, 65);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(100, 20);
            this.nameTextBox.TabIndex = 6;
            // 
            // IDTextBox
            // 
            this.IDTextBox.Location = new System.Drawing.Point(88, 31);
            this.IDTextBox.Name = "IDTextBox";
            this.IDTextBox.Size = new System.Drawing.Size(100, 20);
            this.IDTextBox.TabIndex = 5;
            // 
            // birthdayPicker
            // 
            this.birthdayPicker.Location = new System.Drawing.Point(327, 50);
            this.birthdayPicker.Name = "birthdayPicker";
            this.birthdayPicker.Size = new System.Drawing.Size(200, 20);
            this.birthdayPicker.TabIndex = 4;
            // 
            // birthdayLabel
            // 
            this.birthdayLabel.AutoSize = true;
            this.birthdayLabel.Location = new System.Drawing.Point(324, 31);
            this.birthdayLabel.Name = "birthdayLabel";
            this.birthdayLabel.Size = new System.Drawing.Size(51, 15);
            this.birthdayLabel.TabIndex = 3;
            this.birthdayLabel.Text = "Birthday";
            // 
            // classLabel1
            // 
            this.classLabel1.AutoSize = true;
            this.classLabel1.Location = new System.Drawing.Point(20, 106);
            this.classLabel1.Name = "classLabel1";
            this.classLabel1.Size = new System.Drawing.Size(28, 15);
            this.classLabel1.TabIndex = 2;
            this.classLabel1.Text = "Lớp";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(17, 68);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(44, 15);
            this.nameLabel.TabIndex = 1;
            this.nameLabel.Text = " Name";
            // 
            // IDLabel
            // 
            this.IDLabel.AutoSize = true;
            this.IDLabel.Location = new System.Drawing.Point(20, 31);
            this.IDLabel.Name = "IDLabel";
            this.IDLabel.Size = new System.Drawing.Size(41, 15);
            this.IDLabel.TabIndex = 0;
            this.IDLabel.Text = "MSSV";
            // 
            // actionGroupBox
            // 
            this.actionGroupBox.Controls.Add(this.sortByComboBox);
            this.actionGroupBox.Controls.Add(this.sortButton);
            this.actionGroupBox.Controls.Add(this.deleteButton);
            this.actionGroupBox.Controls.Add(this.editButton);
            this.actionGroupBox.Controls.Add(this.addButton);
            this.actionGroupBox.Controls.Add(this.dataGridView);
            this.actionGroupBox.Controls.Add(this.searchButton);
            this.actionGroupBox.Controls.Add(this.searchTextBox);
            this.actionGroupBox.Controls.Add(this.searchLabel);
            this.actionGroupBox.Controls.Add(this.classComboBox2);
            this.actionGroupBox.Controls.Add(this.classLabel2);
            this.actionGroupBox.Location = new System.Drawing.Point(12, 184);
            this.actionGroupBox.Name = "actionGroupBox";
            this.actionGroupBox.Size = new System.Drawing.Size(573, 258);
            this.actionGroupBox.TabIndex = 2;
            this.actionGroupBox.TabStop = false;
            this.actionGroupBox.Text = "Danh sách SV";
            // 
            // sortByComboBox
            // 
            this.sortByComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sortByComboBox.FormattingEnabled = true;
            this.sortByComboBox.Items.AddRange(new object[] {
            "MSSV",
            "Name",
            "Class",
            "Birthday",
            "Gender"});
            this.sortByComboBox.Location = new System.Drawing.Point(422, 229);
            this.sortByComboBox.Name = "sortByComboBox";
            this.sortByComboBox.Size = new System.Drawing.Size(143, 21);
            this.sortByComboBox.TabIndex = 18;
            // 
            // sortButton
            // 
            this.sortButton.Location = new System.Drawing.Point(320, 227);
            this.sortButton.Name = "sortButton";
            this.sortButton.Size = new System.Drawing.Size(75, 23);
            this.sortButton.TabIndex = 17;
            this.sortButton.Text = "Sort";
            this.sortButton.UseVisualStyleBackColor = true;
            this.sortButton.Click += new System.EventHandler(this.sortButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(222, 227);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 23);
            this.deleteButton.TabIndex = 16;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(126, 227);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(75, 23);
            this.editButton.TabIndex = 15;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(20, 227);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 14;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // dataGridView
            // 
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Location = new System.Drawing.Point(20, 68);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersWidth = 45;
            this.dataGridView.Size = new System.Drawing.Size(545, 155);
            this.dataGridView.TabIndex = 13;
            this.dataGridView.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_RowHeaderMouseClick);
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(490, 30);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(75, 23);
            this.searchButton.TabIndex = 12;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // searchTextBox
            // 
            this.searchTextBox.Location = new System.Drawing.Point(384, 32);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(100, 20);
            this.searchTextBox.TabIndex = 11;
            // 
            // searchLabel
            // 
            this.searchLabel.AutoSize = true;
            this.searchLabel.Location = new System.Drawing.Point(283, 32);
            this.searchLabel.Name = "searchLabel";
            this.searchLabel.Size = new System.Drawing.Size(95, 15);
            this.searchLabel.TabIndex = 10;
            this.searchLabel.Text = "Seach By Name";
            // 
            // classComboBox2
            // 
            this.classComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.classComboBox2.FormattingEnabled = true;
            this.classComboBox2.Location = new System.Drawing.Point(88, 29);
            this.classComboBox2.Name = "classComboBox2";
            this.classComboBox2.Size = new System.Drawing.Size(100, 21);
            this.classComboBox2.TabIndex = 9;
            this.classComboBox2.SelectedIndexChanged += new System.EventHandler(this.classComboBox2_SelectedIndexChanged);
            // 
            // classLabel2
            // 
            this.classLabel2.AutoSize = true;
            this.classLabel2.Location = new System.Drawing.Point(20, 32);
            this.classLabel2.Name = "classLabel2";
            this.classLabel2.Size = new System.Drawing.Size(28, 15);
            this.classLabel2.TabIndex = 2;
            this.classLabel2.Text = "Lớp";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(597, 454);
            this.Controls.Add(this.actionGroupBox);
            this.Controls.Add(this.dataGroupBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.dataGroupBox.ResumeLayout(false);
            this.dataGroupBox.PerformLayout();
            this.genderGroupBox.ResumeLayout(false);
            this.genderGroupBox.PerformLayout();
            this.actionGroupBox.ResumeLayout(false);
            this.actionGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox dataGroupBox;
        private System.Windows.Forms.ComboBox classComboBox1;
        private System.Windows.Forms.GroupBox genderGroupBox;
        private System.Windows.Forms.RadioButton femaleButton;
        private System.Windows.Forms.RadioButton maleButton;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox IDTextBox;
        private System.Windows.Forms.DateTimePicker birthdayPicker;
        private System.Windows.Forms.Label birthdayLabel;
        private System.Windows.Forms.Label classLabel1;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label IDLabel;
        private System.Windows.Forms.GroupBox actionGroupBox;
        private System.Windows.Forms.ComboBox classComboBox2;
        private System.Windows.Forms.Label classLabel2;
        private System.Windows.Forms.ComboBox sortByComboBox;
        private System.Windows.Forms.Button sortButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.TextBox searchTextBox;
        private System.Windows.Forms.Label searchLabel;
    }
}

