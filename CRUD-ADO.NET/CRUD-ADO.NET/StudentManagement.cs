﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUD_ADO.NET
{
    class StudentManagement
    {
        private static StudentManagement _Instance;
        private static string _ConnectionString;

        public static string ConnectionString {
            get
            {
                if(_ConnectionString == null)
                {
                    ConnectionString = @"Data Source=ADMIN;Initial Catalog=SinhVien;User ID=sa;Password=123456";
                }
                return _ConnectionString;
            }
            private set => _ConnectionString = value; 
        }

        public static StudentManagement Instance 
        {
            get 
            {
                if(_Instance == null)
                {
                    _Instance = new StudentManagement();
                }
                return _Instance;
            }
            private set => _Instance = value; 
        
        }
        private StudentManagement()
        {

        }
        public bool Change_Data(string queryString)
        {
            SqlConnection cnn = new SqlConnection(ConnectionString);
            cnn.Open();
            if(cnn.State.ToString() == "Open")
            {
                SqlCommand command = new SqlCommand(queryString, cnn);
                try
                {
                    command.ExecuteNonQuery();
                    return true;
                } catch(Exception error)
                {
                    MessageBox.Show(error.Message);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public DataTable Get_Data(string queryString)
        {
            SqlConnection cnn = new SqlConnection(ConnectionString);
            cnn.Open();
            if (cnn.State.ToString() == "Open")
            {
                SqlCommand command = new SqlCommand(queryString, cnn);
                try
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    DataSet dataset = new DataSet();
                    adapter.Fill(dataset);
                    cnn.Close();
                    return dataset.Tables[0];
                }
                catch(Exception error)
                {
                    cnn.Close();
                    MessageBox.Show(error.Message);
                    return null;
                }
            }
            else
            {
                cnn.Close();
                return null;
            }

        }
    }
}
// Data Reader ( Cần kết nối CSDL để duy trì dữ liệu )
// Dataset ( Cần kết nối CSDL để duy trì dữ liệu )
