﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_ADO.NET
{
    class ComboBoxItem
    {
        private string _Text;
        private string _Value;
        public string Text { get => _Text; set => _Text = value; } // Name
        public string Value { get => _Value; set => _Value = value; } // ID
        // Override ToString to show the value of text to combobox
        public override string ToString()
        {
            return Text.ToString();
        }
    }
}
