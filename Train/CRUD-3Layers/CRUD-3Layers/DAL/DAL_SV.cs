﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRUD_3Layers.DTO;
namespace CRUD_3Layers.DAL
{
    public class DAL_SV
    {
        private static DAL_SV _Instance;

        public static DAL_SV Instance 
        {
            get 
            {
                if(_Instance == null)
                {
                    _Instance = new DAL_SV();
                }
                return _Instance;
            } 
            private set => _Instance = value; 
        }
        private DAL_SV()
        {

        }
        public List<SinhVien> GetSinhVien_DAL()
        {
            string query = "SELECT * FROM SinhVien";
            List<SinhVien> list = new List<SinhVien>();
            foreach (DataRow row in DataProvider.Instance.Get_Data(query).Rows)
            {
                list.Add(GetObjectSV(row));
            }
            return list;
        }
        public SinhVien GetObjectSV(DataRow i)
        {
            return new SinhVien
            {
                StudentID = i["StudentID"].ToString(),
                Name = i["Name"].ToString(),
                ClassID = Convert.ToInt32(i["ClassID"]),
                Birthday = Convert.ToDateTime(i["Birthday"]),
                Gender = Convert.ToInt32(i["Gender"])
            };
        }
        public bool AddSinhVien_DAL(SinhVien sinhvien)
        {
            string query = "INSERT INTO SinhVien (StudentID,Name,ClassID,Birthday,Gender) VALUES ('"
                + sinhvien.StudentID + "','"
                + sinhvien.Name + "','"
                + sinhvien.ClassID + "','"
                + sinhvien.Birthday + "','"
                + sinhvien.Gender + "'" +  
            ")";
            return DataProvider.Instance.Change_Data(query);
        }
    }
}
