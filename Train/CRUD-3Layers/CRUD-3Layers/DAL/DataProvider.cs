﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_3Layers.DAL
{
    class DataProvider
    {
        private static DataProvider _Instance;
        private string _ConnectionString;
        public static DataProvider Instance 
        { 
            get
            {
                if(_Instance == null)
                {
                    _Instance = new DataProvider();
                }
                return _Instance;
            }
            private set => _Instance = value; 
        }

        private DataProvider()
        {
            _ConnectionString = @"Data Source=ADMIN;Initial Catalog=SinhVien;User ID=sa;Password=123456";
        }
        public DataTable Get_Data(string query)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection cnn = new SqlConnection(_ConnectionString))
                {
                    SqlDataAdapter da = new SqlDataAdapter(query, cnn);
                    da.Fill(dt);
                }
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool Change_Data(string query)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(_ConnectionString))
                {
                    cnn.Open();
                    if(cnn.State.ToString() == "Open")
                    {
                        SqlCommand cmd = new SqlCommand(query, cnn);
                        cmd.ExecuteNonQuery();
                    }
                    cnn.Close();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
