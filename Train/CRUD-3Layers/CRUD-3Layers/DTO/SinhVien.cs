﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_3Layers.DTO
{
    public class SinhVien
    {
        private string _StudentID;
        private string _Name;
        private int _ClassID;
        private DateTime _Birthday;
        private int _Gender;

        public string StudentID { get => _StudentID; set => _StudentID = value; }
        public string Name { get => _Name; set => _Name = value; }
        public int ClassID { get => _ClassID; set => _ClassID = value; }
        public DateTime Birthday { get => _Birthday; set => _Birthday = value; }
        public int Gender { get => _Gender; set => _Gender = value; }
    }
}
