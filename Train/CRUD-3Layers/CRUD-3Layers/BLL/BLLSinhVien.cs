﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRUD_3Layers.DTO;
using CRUD_3Layers.DAL;
namespace CRUD_3Layers.BLL
{
    class BLLSinhVien
    {
        private static BLLSinhVien _Instance;

        public static BLLSinhVien Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new BLLSinhVien();
                }
                return _Instance;
            }
            private set => _Instance = value;
        }
        private BLLSinhVien()
        {

        }
        public List<SinhVien> GetSinhVien_BLL()
        {
            // return list SV , lấy từ Data Access Layer, nhiệm vụ của DAL là chuyển tập hợp SV -> List SV
            // Ví dụ tên hàm tại DAL là GetListSV_DAL 
            return DAL_SV.Instance.GetSinhVien_DAL();
        }
        public bool AddSinhVien_BLL(SinhVien sinhvien)
        {
            return DAL_SV.Instance.AddSinhVien_DAL(sinhvien);
        }
    }
}
