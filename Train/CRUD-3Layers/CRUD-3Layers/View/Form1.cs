﻿using CRUD_3Layers.BLL;
using CRUD_3Layers.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUD_3Layers
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = BLLSinhVien.Instance.GetSinhVien_BLL();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string StudentID = "102170200";
            string Name = "Nguyen Quang Tu";
            int ClassID = 2;
            DateTime Birthday = Convert.ToDateTime("1992-12-1");
            int Gender = 1;
            if(BLLSinhVien.Instance.AddSinhVien_BLL(new SinhVien { StudentID = StudentID, Name = Name, ClassID = ClassID, Birthday = Birthday, Gender = Gender }))
            {
                dataGridView1.DataSource = BLLSinhVien.Instance.GetSinhVien_BLL();
            }
            else
            {
                MessageBox.Show("Error while adding record");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
        }
    }
}
