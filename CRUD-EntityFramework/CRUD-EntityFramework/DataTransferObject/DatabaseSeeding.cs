﻿using CRUD_EntityFramework.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_EntityFramework.DataTransferObject
{
    public class DatabaseSeeding : CreateDatabaseIfNotExists<EmployeeMangement>
    {
        // Protected vì không được phép thay đổi bổ sung truy cập của nó
        protected override void Seed(EmployeeMangement context) // context là đối tượng CSDL
        {
            // Nếu add người dùng trước thì khi biên dịch nó cũng sẽ tìm thằng office để add trước
            context.ListEmployee.Add(new Employee
            {
                EmployeeID = 1,
                EmployeeName = "Nguyen Quang Tu",
                DateJoin = Convert.ToDateTime("2020-12-20"),
                OfficeID = 1,
            });
            context.ListEmployee.Add(new Employee
            {
                EmployeeID = 2,
                EmployeeName = "Nguyen Quang Tuan",
                DateJoin = Convert.ToDateTime("2020-10-20"),
                OfficeID = 2,
            });
            context.ListEmployee.Add(new Employee
            {
                EmployeeID = 3,
                EmployeeName = "Nguyen Quang Huy",
                DateJoin = Convert.ToDateTime("2020-11-20"),
                OfficeID = 2,
            });
            context.ListOffice.Add(new Office
            {
                OfficeID = 1,
                OfficeName = "Van phong A",
            });
            context.ListOffice.Add(new Office
            {
                OfficeID = 2,
                OfficeName = "Van phong B",
            });
            context.ListOffice.Add(new Office
            {
                OfficeID =3,
                OfficeName = "Van phong C",
            });
        }
    }
}
