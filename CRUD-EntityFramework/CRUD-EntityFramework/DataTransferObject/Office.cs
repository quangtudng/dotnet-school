﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_EntityFramework.DataTransferObject
{
    public class Office
    {
        [Key]
        public int OfficeID { get; set; }
        [StringLength(50)][Required]
        public string OfficeName { get; set; }
        public virtual ICollection<Employee> EmployeeNV { get; set; } // Thuộc tính quan hệ
        // Đối tượng Office phải luôn tạo trước Employee
        public Office()
        {
            // Đảm bảo thuộc tính quan hệ của đối tượng Office luôn tồn tại
            this.EmployeeNV = new HashSet<Employee>();
        }
    }
}
