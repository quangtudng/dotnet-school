﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_EntityFramework.DataTransferObject
{
    [Table("Employee")]
    public class Employee
    {
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public DateTime DateJoin { get; set; }
        public int OfficeID { get; set; }
        [ForeignKey("OfficeID")]
        // Thuộc tính dưới này sẽ được thiết lập thông qua khoá ngoại
        public virtual Office OfficeNV { get; set; }
    }
}
