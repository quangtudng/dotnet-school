﻿namespace CRUD_EntityFramework.DataAccessLayer
{
    using CRUD_EntityFramework.DataTransferObject;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class EmployeeMangement : DbContext
    {
        private static EmployeeMangement _Instance;
        public static EmployeeMangement Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new EmployeeMangement();
                }
                return _Instance;
            }
            private set => _Instance = value;
        }
        // Your context has been configured to use a 'EmployeeMangement' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'CRUD_EntityFramework.DataAccessLayer.EmployeeMangement' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'EmployeeMangement' 
        // connection string in the application configuration file.
        private EmployeeMangement()
            : base("name=EmployeeMangement")
        {
            // Phương thức này sẽ tìm tới DatabaseSeeding và tìm gọi tới method Seed
            Database.SetInitializer(new DatabaseSeeding());
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Office> ListOffice { get; set; }
        public virtual DbSet<Employee> ListEmployee { get; set; }
    }
}