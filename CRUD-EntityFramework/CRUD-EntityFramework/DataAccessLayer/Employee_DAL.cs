﻿using CRUD_EntityFramework.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_EntityFramework.DataAccessLayer
{
    public class Employee_DAL
    {
        private static Employee_DAL _Instance;
        public static Employee_DAL Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new Employee_DAL();
                }
                return _Instance;
            }
            private set => _Instance = value;
        }
        private Employee_DAL()
        {

        }
        public List<Employee> GetAllEmployees_DAL()
        {
            var list = EmployeeMangement.Instance.ListEmployee.Select(p => p);
            return list.ToList<Employee>();
        }
    }
}
