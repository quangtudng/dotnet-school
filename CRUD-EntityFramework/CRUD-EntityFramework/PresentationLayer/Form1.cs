﻿using CRUD_EntityFramework.BusinessLogicLayer;
using CRUD_EntityFramework.DataAccessLayer;
using CRUD_EntityFramework.DataTransferObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUD_EntityFramework
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            dataGridView.DataSource = Employee_BLL.Instance.GetAllEmployees_BLL();
        }
    }
}
