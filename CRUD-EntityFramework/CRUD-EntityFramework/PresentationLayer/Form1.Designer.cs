﻿namespace CRUD_EntityFramework
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.actionGroupBox = new System.Windows.Forms.GroupBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.showButton = new System.Windows.Forms.Button();
            this.selectLabel = new System.Windows.Forms.Label();
            this.officeComboBox2 = new System.Windows.Forms.ComboBox();
            this.sortButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.createButton = new System.Windows.Forms.Button();
            this.listGroupBox = new System.Windows.Forms.GroupBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.infoGroupBox = new System.Windows.Forms.GroupBox();
            this.receivedDatePicker = new System.Windows.Forms.DateTimePicker();
            this.dateLabel = new System.Windows.Forms.Label();
            this.officeComboBox1 = new System.Windows.Forms.ComboBox();
            this.categoryLabel = new System.Windows.Forms.Label();
            this.employeeNameTextBox = new System.Windows.Forms.TextBox();
            this.employeeNameLabel = new System.Windows.Forms.Label();
            this.employeeIDTextBox = new System.Windows.Forms.TextBox();
            this.employeeIDLabel = new System.Windows.Forms.Label();
            this.actionGroupBox.SuspendLayout();
            this.listGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.infoGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // actionGroupBox
            // 
            this.actionGroupBox.Controls.Add(this.searchButton);
            this.actionGroupBox.Controls.Add(this.searchTextBox);
            this.actionGroupBox.Controls.Add(this.showButton);
            this.actionGroupBox.Controls.Add(this.selectLabel);
            this.actionGroupBox.Controls.Add(this.officeComboBox2);
            this.actionGroupBox.Controls.Add(this.sortButton);
            this.actionGroupBox.Controls.Add(this.deleteButton);
            this.actionGroupBox.Controls.Add(this.updateButton);
            this.actionGroupBox.Controls.Add(this.createButton);
            this.actionGroupBox.Location = new System.Drawing.Point(547, 10);
            this.actionGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.actionGroupBox.Name = "actionGroupBox";
            this.actionGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.actionGroupBox.Size = new System.Drawing.Size(352, 226);
            this.actionGroupBox.TabIndex = 5;
            this.actionGroupBox.TabStop = false;
            this.actionGroupBox.Text = "Action";
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(244, 167);
            this.searchButton.Margin = new System.Windows.Forms.Padding(4);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(100, 28);
            this.searchButton.TabIndex = 12;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            // 
            // searchTextBox
            // 
            this.searchTextBox.Location = new System.Drawing.Point(12, 170);
            this.searchTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(223, 22);
            this.searchTextBox.TabIndex = 11;
            // 
            // showButton
            // 
            this.showButton.Location = new System.Drawing.Point(244, 130);
            this.showButton.Margin = new System.Windows.Forms.Padding(4);
            this.showButton.Name = "showButton";
            this.showButton.Size = new System.Drawing.Size(100, 28);
            this.showButton.TabIndex = 6;
            this.showButton.Text = "Show";
            this.showButton.UseVisualStyleBackColor = true;
            this.showButton.Click += new System.EventHandler(this.showButton_Click);
            // 
            // selectLabel
            // 
            this.selectLabel.AutoSize = true;
            this.selectLabel.Location = new System.Drawing.Point(8, 108);
            this.selectLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.selectLabel.Name = "selectLabel";
            this.selectLabel.Size = new System.Drawing.Size(124, 17);
            this.selectLabel.TabIndex = 5;
            this.selectLabel.Text = "Selected Category";
            // 
            // officeComboBox2
            // 
            this.officeComboBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.officeComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.officeComboBox2.FormattingEnabled = true;
            this.officeComboBox2.Location = new System.Drawing.Point(12, 130);
            this.officeComboBox2.Margin = new System.Windows.Forms.Padding(4);
            this.officeComboBox2.Name = "officeComboBox2";
            this.officeComboBox2.Size = new System.Drawing.Size(223, 24);
            this.officeComboBox2.TabIndex = 4;
            // 
            // sortButton
            // 
            this.sortButton.Location = new System.Drawing.Point(201, 79);
            this.sortButton.Margin = new System.Windows.Forms.Padding(4);
            this.sortButton.Name = "sortButton";
            this.sortButton.Size = new System.Drawing.Size(143, 28);
            this.sortButton.TabIndex = 3;
            this.sortButton.Text = "Sort";
            this.sortButton.UseVisualStyleBackColor = true;
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(8, 76);
            this.deleteButton.Margin = new System.Windows.Forms.Padding(4);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(141, 28);
            this.deleteButton.TabIndex = 2;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(201, 41);
            this.updateButton.Margin = new System.Windows.Forms.Padding(4);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(143, 28);
            this.updateButton.TabIndex = 1;
            this.updateButton.Text = "Update";
            this.updateButton.UseVisualStyleBackColor = true;
            // 
            // createButton
            // 
            this.createButton.Location = new System.Drawing.Point(8, 41);
            this.createButton.Margin = new System.Windows.Forms.Padding(4);
            this.createButton.Name = "createButton";
            this.createButton.Size = new System.Drawing.Size(141, 28);
            this.createButton.TabIndex = 0;
            this.createButton.Text = "Create";
            this.createButton.UseVisualStyleBackColor = true;
            // 
            // listGroupBox
            // 
            this.listGroupBox.Controls.Add(this.dataGridView);
            this.listGroupBox.Location = new System.Drawing.Point(39, 244);
            this.listGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.listGroupBox.Name = "listGroupBox";
            this.listGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.listGroupBox.Size = new System.Drawing.Size(860, 260);
            this.listGroupBox.TabIndex = 4;
            this.listGroupBox.TabStop = false;
            this.listGroupBox.Text = "Employee List";
            // 
            // dataGridView
            // 
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Location = new System.Drawing.Point(8, 23);
            this.dataGridView.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersWidth = 45;
            this.dataGridView.Size = new System.Drawing.Size(844, 220);
            this.dataGridView.TabIndex = 0;
            // 
            // infoGroupBox
            // 
            this.infoGroupBox.Controls.Add(this.receivedDatePicker);
            this.infoGroupBox.Controls.Add(this.dateLabel);
            this.infoGroupBox.Controls.Add(this.officeComboBox1);
            this.infoGroupBox.Controls.Add(this.categoryLabel);
            this.infoGroupBox.Controls.Add(this.employeeNameTextBox);
            this.infoGroupBox.Controls.Add(this.employeeNameLabel);
            this.infoGroupBox.Controls.Add(this.employeeIDTextBox);
            this.infoGroupBox.Controls.Add(this.employeeIDLabel);
            this.infoGroupBox.Location = new System.Drawing.Point(39, 9);
            this.infoGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.infoGroupBox.Name = "infoGroupBox";
            this.infoGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.infoGroupBox.Size = new System.Drawing.Size(500, 228);
            this.infoGroupBox.TabIndex = 3;
            this.infoGroupBox.TabStop = false;
            this.infoGroupBox.Text = "Employee Information";
            // 
            // receivedDatePicker
            // 
            this.receivedDatePicker.Location = new System.Drawing.Point(128, 162);
            this.receivedDatePicker.Margin = new System.Windows.Forms.Padding(4);
            this.receivedDatePicker.Name = "receivedDatePicker";
            this.receivedDatePicker.Size = new System.Drawing.Size(247, 22);
            this.receivedDatePicker.TabIndex = 9;
            // 
            // dateLabel
            // 
            this.dateLabel.AutoSize = true;
            this.dateLabel.Location = new System.Drawing.Point(8, 167);
            this.dateLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(68, 17);
            this.dateLabel.TabIndex = 8;
            this.dateLabel.Text = "Join Date";
            // 
            // officeComboBox1
            // 
            this.officeComboBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.officeComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.officeComboBox1.FormattingEnabled = true;
            this.officeComboBox1.Location = new System.Drawing.Point(128, 121);
            this.officeComboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.officeComboBox1.Name = "officeComboBox1";
            this.officeComboBox1.Size = new System.Drawing.Size(247, 24);
            this.officeComboBox1.TabIndex = 7;
            // 
            // categoryLabel
            // 
            this.categoryLabel.AutoSize = true;
            this.categoryLabel.Location = new System.Drawing.Point(9, 124);
            this.categoryLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.categoryLabel.Name = "categoryLabel";
            this.categoryLabel.Size = new System.Drawing.Size(86, 17);
            this.categoryLabel.TabIndex = 4;
            this.categoryLabel.Text = "Office Name";
            // 
            // employeeNameTextBox
            // 
            this.employeeNameTextBox.Location = new System.Drawing.Point(128, 75);
            this.employeeNameTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.employeeNameTextBox.Name = "employeeNameTextBox";
            this.employeeNameTextBox.Size = new System.Drawing.Size(247, 22);
            this.employeeNameTextBox.TabIndex = 3;
            // 
            // employeeNameLabel
            // 
            this.employeeNameLabel.AutoSize = true;
            this.employeeNameLabel.Location = new System.Drawing.Point(9, 78);
            this.employeeNameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.employeeNameLabel.Name = "employeeNameLabel";
            this.employeeNameLabel.Size = new System.Drawing.Size(111, 17);
            this.employeeNameLabel.TabIndex = 2;
            this.employeeNameLabel.Text = "Employee Name";
            // 
            // employeeIDTextBox
            // 
            this.employeeIDTextBox.Location = new System.Drawing.Point(128, 28);
            this.employeeIDTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.employeeIDTextBox.Name = "employeeIDTextBox";
            this.employeeIDTextBox.Size = new System.Drawing.Size(247, 22);
            this.employeeIDTextBox.TabIndex = 1;
            // 
            // employeeIDLabel
            // 
            this.employeeIDLabel.AutoSize = true;
            this.employeeIDLabel.Location = new System.Drawing.Point(9, 31);
            this.employeeIDLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.employeeIDLabel.Name = "employeeIDLabel";
            this.employeeIDLabel.Size = new System.Drawing.Size(87, 17);
            this.employeeIDLabel.TabIndex = 0;
            this.employeeIDLabel.Text = "Employee ID";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 513);
            this.Controls.Add(this.actionGroupBox);
            this.Controls.Add(this.listGroupBox);
            this.Controls.Add(this.infoGroupBox);
            this.Name = "Form1";
            this.actionGroupBox.ResumeLayout(false);
            this.actionGroupBox.PerformLayout();
            this.listGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.infoGroupBox.ResumeLayout(false);
            this.infoGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox actionGroupBox;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.TextBox searchTextBox;
        private System.Windows.Forms.Button showButton;
        private System.Windows.Forms.Label selectLabel;
        private System.Windows.Forms.ComboBox officeComboBox2;
        private System.Windows.Forms.Button sortButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Button createButton;
        private System.Windows.Forms.GroupBox listGroupBox;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.GroupBox infoGroupBox;
        private System.Windows.Forms.DateTimePicker receivedDatePicker;
        private System.Windows.Forms.Label dateLabel;
        private System.Windows.Forms.ComboBox officeComboBox1;
        private System.Windows.Forms.Label categoryLabel;
        private System.Windows.Forms.TextBox employeeNameTextBox;
        private System.Windows.Forms.Label employeeNameLabel;
        private System.Windows.Forms.TextBox employeeIDTextBox;
        private System.Windows.Forms.Label employeeIDLabel;
    }
}

