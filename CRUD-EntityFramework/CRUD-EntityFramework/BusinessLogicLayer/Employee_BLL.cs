﻿using CRUD_EntityFramework.DataAccessLayer;
using CRUD_EntityFramework.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_EntityFramework.BusinessLogicLayer
{
    public class Employee_BLL
    {
        private static Employee_BLL _Instance;
        public static Employee_BLL Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new Employee_BLL();
                }
                return _Instance;
            }
            private set => _Instance = value;
        }
        private Employee_BLL()
        {

        }
        public List<Employee> GetAllEmployees_BLL()
        {
            return Employee_DAL.Instance.GetAllEmployees_DAL();
        }
    }
}
