CREATE TABLE Categories(
    CategoryID int primary key,
    CategoryName varchar(255),
)
CREATE TABLE Books(
    BookID varchar(255) primary key,
    BookName varchar(255),
    CategoryID int foreign key references Categories(CategoryID),
    DateOfReceived DATE,
    LicenseChecked BIT,
)
INSERT INTO Categories(CategoryID, CategoryName) VALUES
    (1, 'History'),
    (2, 'Detective'),
    (3, 'Entertainment'),
    (4, 'Comic books'),
    (5, 'Reference Books')
INSERT INTO Books(BookID, BookName, CategoryID, DateOfReceived,LicenseChecked) VALUES
    ('BK1001', 'World War II', 1, '2020-05-12', 1),
    ('BK1532', 'Sherlock Holmes', 2, '2019-02-20', 0),
    ('BK2322', 'Data Structure', 5, '2018-12-04', 1),
    ('BK4322', 'Computer Science', 5, '2020-04-03', 0),
    ('BK1562', 'Vietnam War', 1, '2020-12-03', 0),
    ('BK7112', 'A Great Tale', 1, '2017-11-23', 1),
    ('BK2312', 'The White Wolf', 1, '2014-02-05', 1),
    ('BK5122', 'Fate Unlimited Blade Work', 4, '2010-03-03', 1),
    ('BK1231', 'Fate Zero', 4, '2010-04-03', 0),
    ('BK9212', 'Fate Grand Order', 4, '2015-01-20', 1)