﻿using _102170257_NguyenQuangTu.DataAccessLayer;
using _102170257_NguyenQuangTu.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _102170257_NguyenQuangTu.BusinessLogicLayer
{
    public class Book_BLL
    {
        private static Book_BLL _Instance;

        public static Book_BLL Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new Book_BLL();
                }
                return _Instance;
            }
            private set => _Instance = value;
        }
        private Book_BLL()
        {

        }
        // Get Methods
        public List<Books> GetAllBooks_BLL()
        {
            return Book_DAL.Instance.GetAllBooks_DAL();
        }
        public List<Books> GetBooksByCategory_BLL(int categoryID)
        {
            return Book_DAL.Instance.GetBooksByCategory_DAL(categoryID);
        }
        public List<Books> GetBooksByName_BLL(string BookName)
        {
            return Book_DAL.Instance.GetBooksByName_DAL(BookName);
        }
        public Books GetBookByID_BLL(string BookID)
        {
            return Book_DAL.Instance.GetBookByID_DAL(BookID)[0];
        }
        // Add Method
        public bool AddBook_BLL(string BookID, string BookName, int CategoryID, DateTime DateOfReceived, bool LicenseChecked)
        {
            return Book_DAL.Instance.AddBook_DAL(new Books
            {
                BookID = BookID,
                BookName = BookName,
                CategoryID = CategoryID,
                DateOfReceived = DateOfReceived,
                LicenseChecked = LicenseChecked
            });
        }
        // Edit Method
        public bool EditBook_BLL(string BookID, string BookName, int CategoryID, DateTime DateOfReceived, bool LicenseChecked)
        {
            return Book_DAL.Instance.EditBook_DAL(new Books
            {
                BookID = BookID,
                BookName = BookName,
                CategoryID = CategoryID,
                DateOfReceived = DateOfReceived,
                LicenseChecked = LicenseChecked
            });
        }
        // Delete Method
        public bool DeleteBookByID_BLL(string BookID)
        {
            return Book_DAL.Instance.DeleteBookByID_DAL(BookID);
        }
        // Sort Method
        public List<Books> SortBookByID_BLL(List<Books> listBook)
        {
            listBook = listBook.OrderBy(o => o.BookID).ToList();
            return listBook;
        }
    }
}
