﻿using _102170257_NguyenQuangTu.DataAccessLayer;
using _102170257_NguyenQuangTu.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _102170257_NguyenQuangTu.BusinessLogicLayer
{
    public class Category_BLL
    {
        private static Category_BLL _Instance;

        public static Category_BLL Instance 
        {
            get 
            {
                if(_Instance == null)
                {
                    _Instance = new Category_BLL();
                }
                return _Instance;
            } 
            private set => _Instance = value; 
        }
        private Category_BLL()
        {

        }
        public List<Categories> GetAllCategories_BLL()
        {
            return Category_DAL.Instance.GetAllCategories_DAL();
        }
    }
}
