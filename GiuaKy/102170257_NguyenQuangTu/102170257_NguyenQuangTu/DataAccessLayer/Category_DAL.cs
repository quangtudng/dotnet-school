﻿using _102170257_NguyenQuangTu.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _102170257_NguyenQuangTu.DataAccessLayer
{
    public class Category_DAL
    {
        private static Category_DAL _Instance;

        public static Category_DAL Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new Category_DAL();
                }
                return _Instance;
            }
            private set => _Instance = value;
        }
        private Category_DAL()
        {

        }
        public List<Categories> GetAllCategories_DAL()
        {
            string query = "SELECT * FROM Categories";
            List<Categories> list_category = new List<Categories>();
            foreach (DataRow i in BookManagement.Instance.Get_Database(query).Rows)
            {
                list_category.Add(GetObjectCategory(i));
            }
            return list_category;
        }
        public Categories GetObjectCategory(DataRow i)
        {
            return new Categories
            {
                CategoryID = Convert.ToInt32(i["CategoryID"]),
                CategoryName = i["CategoryName"].ToString()
            };
        }
    }
}
