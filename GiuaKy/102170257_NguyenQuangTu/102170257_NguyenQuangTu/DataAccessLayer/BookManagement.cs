﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _102170257_NguyenQuangTu.DataAccessLayer
{
    public class BookManagement
    {
        private static BookManagement _Instance;
        private string _ConnectionString;
        public static BookManagement Instance 
        {
            get 
            {
                if(_Instance == null)
                {
                    _Instance = new BookManagement();
                }
                return _Instance;
            } 
            private set => _Instance = value; 
        }
        public string ConnectionString 
        { 
            get 
            {
                if(_ConnectionString == null)
                {
                    _ConnectionString = "Data Source=ADMIN;Initial Catalog=LibraryManagement;User ID=sa;Password=123456";
                }
                return _ConnectionString;
            } 
            private set => _ConnectionString = value; 
        }
        private BookManagement()
        {

        }
        public bool Change_Database(string query)
        {
            try
            {
                SqlConnection connection = new SqlConnection(ConnectionString);
                connection.Open();
                if(connection.State.ToString() == "Open")
                {
                    SqlCommand command = new SqlCommand(query, connection);
                    command.ExecuteNonQuery();
                    connection.Close();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception) 
            {
                return false;
            }
        }
        public DataTable Get_Database(string query)
        {
            try
            {
                SqlConnection connection = new SqlConnection(ConnectionString);
                connection.Open();
                if(connection.State.ToString() == "Open")
                {
                    DataSet dataset = new DataSet();
                    SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
                    adapter.Fill(dataset);
                    connection.Close();
                    return dataset.Tables[0];
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
