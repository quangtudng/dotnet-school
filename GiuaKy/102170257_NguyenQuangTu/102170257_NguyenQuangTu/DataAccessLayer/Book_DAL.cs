﻿using _102170257_NguyenQuangTu.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _102170257_NguyenQuangTu.DataAccessLayer
{
    public class Book_DAL
    {
        private static Book_DAL _Instance;
        public static Book_DAL Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new Book_DAL();
                }
                return _Instance;
            }
            private set => _Instance = value;
        }
        private Book_DAL()
        {

        }
        public Books GetObjectBooks(DataRow i)
        {
            return new Books
            {
                BookID = i["BookID"].ToString(),
                BookName = i["BookName"].ToString(),
                CategoryName = i["CategoryName"].ToString(),
                DateOfReceived = Convert.ToDateTime(i["DateOfReceived"]),
                LicenseChecked = Convert.ToBoolean(i["LicenseChecked"]),
                CategoryID = Convert.ToInt32(i["CategoryID"]),
            };
        }
        // Get Methods
        public List<Books> GetAllBooks_DAL()
        {
            string query = "SELECT * FROM Books JOIN Categories ON Books.CategoryID=Categories.CategoryID";
            List<Books> list_book = new List<Books>();
            foreach (DataRow i in BookManagement.Instance.Get_Database(query).Rows)
            {
                list_book.Add(GetObjectBooks(i));
            }
            return list_book;
        }
        public List<Books> GetBooksByCategory_DAL(int categoryID)
        {
            string query = "SELECT * FROM Books JOIN Categories ON Books.CategoryID = Categories.CategoryID WHERE Books.CategoryID=" + categoryID;
            List<Books> list_book = new List<Books>();
            foreach (DataRow i in BookManagement.Instance.Get_Database(query).Rows)
            {
                list_book.Add(GetObjectBooks(i));
            }
            return list_book;
        }
        public List<Books> GetBooksByName_DAL(string BookName)
        {
            string query = "SELECT * FROM Books JOIN Categories ON Books.CategoryID = Categories.CategoryID WHERE BookName LIKE '" + BookName + "%'";
            List<Books> list_book = new List<Books>();
            foreach (DataRow i in BookManagement.Instance.Get_Database(query).Rows)
            {
                list_book.Add(GetObjectBooks(i));
            }
            return list_book;
        }
        public List<Books> GetBookByID_DAL(string BookID)
        {
            string query = "SELECT * FROM Books JOIN Categories ON Books.CategoryID = Categories.CategoryID WHERE BookID='" + BookID + "'";
            List<Books> list_book = new List<Books>();
            foreach (DataRow i in BookManagement.Instance.Get_Database(query).Rows)
            {
                list_book.Add(GetObjectBooks(i));
            }
            return list_book;
        }
        // Add Method
        public bool AddBook_DAL(Books book)
        {
            string query = "INSERT INTO Books(BookID,BookName,CategoryID,DateOfReceived,LicenseChecked) VALUES " +
             "('" +
                 book.BookID + "','" +
                 book.BookName + "','" +
                 book.CategoryID + "','" +
                 book.DateOfReceived + "','" +
                 book.LicenseChecked +
             "')";
            try
            {
                return BookManagement.Instance.Change_Database(query);
            }
            catch (Exception)
            {
                return false;
            }
        }
        // Edit Method
        public bool EditBook_DAL(Books book)
        {
            int license = 1;
            if (book.LicenseChecked)
            {
                license = 1;
            }
            else
            {
                license = 0;
            }
            string query = "UPDATE Books SET BookName='"
                + book.BookName + "',CategoryID='"
                + book.CategoryID + "',DateOfReceived='"
                + book.DateOfReceived + "',LicenseChecked="
                + license + " WHERE BookID='"
                + book.BookID + "'";
            try
            {
                return BookManagement.Instance.Change_Database(query);
            }
            catch (Exception)
            {
                return false;
            }
        }
        // Delete Method
        public bool DeleteBookByID_DAL(string BookID)
        {
            return BookManagement.Instance.Change_Database("DELETE FROM Books WHERE BookID='" + BookID + "'");
        }
    }
}
