﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _102170257_NguyenQuangTu.DataTransferObject
{
    public class Categories
    {
        private int _CategoryID;
        private string _CategoryName;

        public int CategoryID { get => _CategoryID; set => _CategoryID = value; }
        public string CategoryName { get => _CategoryName; set => _CategoryName = value; }
        public override string ToString()
        {
            return CategoryName;
        }
    }
}
