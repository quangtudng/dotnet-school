﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _102170257_NguyenQuangTu.DataTransferObject
{
    public class Books
    {
        private string _BookID;
        private int _CategoryID;
        private string _BookName;
        // Thuộc tính sau khi join
        private string _CategoryName;
        private DateTime _DateOfReceived;
        private bool _LicenseChecked;

        public string BookID { get => _BookID; set => _BookID = value; }
        public int CategoryID { get => _CategoryID; set => _CategoryID = value; }
        public string BookName { get => _BookName; set => _BookName = value; }
        public string CategoryName { get => _CategoryName; set => _CategoryName = value; }
        public DateTime DateOfReceived { get => _DateOfReceived; set => _DateOfReceived = value; }
        public bool LicenseChecked { get => _LicenseChecked; set => _LicenseChecked = value; }
    }
}
