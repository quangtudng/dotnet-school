﻿using _102170257_NguyenQuangTu.BusinessLogicLayer;
using _102170257_NguyenQuangTu.DataTransferObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _102170257_NguyenQuangTu
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            InitCombobox();
        }
        public void InitCombobox()
        {
            categoryComboBox2.Items.Add("All");
            foreach (Categories category in Category_BLL.Instance.GetAllCategories_BLL())
            {
                categoryComboBox1.Items.Add(category);
                categoryComboBox2.Items.Add(category);
            }
        }
        private void showButton_Click(object sender, EventArgs e)
        {
            if(categoryComboBox2.SelectedItem == null)
            {
                MessageBox.Show("Please select a category to show");
            }
            else if(categoryComboBox2.SelectedItem.ToString() == "All")
            {
                dataGridView.DataSource = Book_BLL.Instance.GetAllBooks_BLL();
            }
            else
            {
                dataGridView.DataSource = Book_BLL.Instance.GetBooksByCategory_BLL(((Categories)categoryComboBox2.SelectedItem).CategoryID);
            }
        }
        private void dataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dataGridView.SelectedRows.Count == 1)
            {
                Books book = Book_BLL.Instance.GetBookByID_BLL(dataGridView.SelectedRows[0].Cells["BookID"].Value.ToString());
                // Fill information
                bookIDTextBox.Text = book.BookID;
                bookNameTextBox.Text = book.BookName;
                for (int i = 0; i < categoryComboBox1.Items.Count; i++)
                {
                    if (((Categories)categoryComboBox1.Items[i]).CategoryID == book.CategoryID)
                    {
                        categoryComboBox1.SelectedIndex = i;
                    }
                }
                receivedDatePicker.Value = book.DateOfReceived;
                if (book.LicenseChecked)
                {
                    yesButton.Checked = true;
                }
                else
                {
                    noButton.Checked = true;
                }
            }
        }
        private void createButton_Click(object sender, EventArgs e)
        {
            string BookID = bookIDTextBox.Text;
            string BookName = bookNameTextBox.Text;
            int CategoryID = ((Categories)categoryComboBox1.SelectedItem).CategoryID;
            DateTime DateOfReceived = Convert.ToDateTime(receivedDatePicker.Value.Date.ToShortDateString());
            bool License = true;
            if (yesButton.Checked)
            {
                License = true;
            }
            else if (noButton.Checked)
            {
                License = false;
            }
            if (!Book_BLL.Instance.AddBook_BLL(BookID, BookName, CategoryID, DateOfReceived, License))
            {
                MessageBox.Show("An error has occurred during adding user. Please try again");
            }
            categoryComboBox2.SelectedIndex = 0;
            dataGridView.DataSource = Book_BLL.Instance.GetAllBooks_BLL();
        }
        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (!Book_BLL.Instance.DeleteBookByID_BLL(bookIDTextBox.Text))
            {
                MessageBox.Show("An error has occured during deleting user");
            }
            categoryComboBox2.SelectedIndex = 0;
            dataGridView.DataSource = Book_BLL.Instance.GetAllBooks_BLL();
        }
        private void updateButton_Click(object sender, EventArgs e)
        {
            string BookID = bookIDTextBox.Text;
            string BookName = bookNameTextBox.Text;
            int CategoryID = ((Categories)categoryComboBox1.SelectedItem).CategoryID;
            DateTime DateOfReceived = Convert.ToDateTime(receivedDatePicker.Value.Date.ToShortDateString());
            bool license = true;
            if (yesButton.Checked)
            {
                license = true;
            }
            else if (noButton.Checked)
            {
                license = false;
            }
            if (!Book_BLL.Instance.EditBook_BLL(BookID, BookName, CategoryID, DateOfReceived, license))
            {
                MessageBox.Show("An error has occurred during editing user");
            }
            categoryComboBox2.SelectedIndex = 0;
            dataGridView.DataSource = Book_BLL.Instance.GetAllBooks_BLL();
        }
        private void searchButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(searchTextBox.Text))
            {
                categoryComboBox2.SelectedIndex = 0;
                dataGridView.DataSource = Book_BLL.Instance.GetAllBooks_BLL();
            }
            else
            {
                dataGridView.DataSource = Book_BLL.Instance.GetBooksByName_BLL(searchTextBox.Text);
            }
        }
        private void sortButton_Click(object sender, EventArgs e)
        {
            dataGridView.DataSource = Book_BLL.Instance.SortBookByID_BLL((List<Books>)dataGridView.DataSource);
        }
    }
}
