﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_3Layers.DataAccessLayer
{
    class DataProvider
    {
        private static DataProvider _Instance;
        private string _ConnectionString;
        public static DataProvider Instance 
        {
            get 
            {
                if(_Instance == null)
                {
                    _Instance = new DataProvider();
                }
                return _Instance;
            } 
            private set => _Instance = value; 
        }

        public string ConnectionString 
        {   
            get
            {
                if(_ConnectionString == null)
                {
                    _ConnectionString = @"Data Source=ADMIN;Initial Catalog=SinhVien;User ID=sa;Password=123456";
                }
                return _ConnectionString;
            } 
            private set => _ConnectionString = value; 
        }

        private DataProvider()
        {

        }
        public bool Change_Data(string query)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            connection.Open();
            if(connection.State.ToString() == "Open")
            {
                try
                {
                    SqlCommand command = new SqlCommand(query, connection);
                    command.ExecuteNonQuery();
                    connection.Close();
                    return true;
                }
                catch(Exception)
                {
                    connection.Close();
                    return false;
                }
            }
            else 
            {
                return false;
            }
        }
        public DataTable Get_Data(string query)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            connection.Open();
            if(connection.State.ToString() == "Open")
            {
                DataSet dataset = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
                adapter.Fill(dataset);
                connection.Close();
                return dataset.Tables[0];
            }
            else
            {
                connection.Close();
                return null;
            }
        }
    }
}
