﻿using CRUD_3Layers.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_3Layers.DataAccessLayer
{
    public class Class_DAL
    {
        private static Class_DAL _Instance;

        public static Class_DAL Instance 
        { 
            get
            {
                if(_Instance == null)
                {
                    _Instance = new Class_DAL();
                }
                return _Instance;
            } 
            private set => _Instance = value; 
        }
        private Class_DAL()
        {

        }
        public List<Class> GetAllClass_DAL()
        {
            string query = "SELECT * FROM Class";
            List<Class> classList = new List<Class>();
            foreach (DataRow i in DataProvider.Instance.Get_Data(query).Rows) 
            {
                classList.Add(GetObjectClass(i));
            }
            return classList;
        }
        public Class GetObjectClass(DataRow i)
        {
            return new Class
            {
                ClassID = Convert.ToInt32(i["ClassID"]),
                Name = i["Name"].ToString()
            };
        }
    }
}
