﻿using CRUD_3Layers.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_3Layers.DataAccessLayer
{
    public class SinhVien_DAL
    {
        private static SinhVien_DAL _Instance;

        public static SinhVien_DAL Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new SinhVien_DAL();
                }
                return _Instance;
            }
            private set => _Instance = value;
        }
        private SinhVien_DAL()
        {

        }
        public SinhVien GetObjectSinhVien(DataRow i)
        {
            return new SinhVien
            {
                StudentID = i["StudentID"].ToString(),
                Name = i["Name"].ToString(),
                ClassID = Convert.ToInt32(i["ClassID"]),
                Birthday = Convert.ToDateTime(i["Birthday"]),
                Gender = Convert.ToBoolean(i["Gender"])
            };
        }
        public List<SinhVien> GetAllSinhVien_DAL()
        {
            string query = "SELECT * FROM SinhVien";
            List<SinhVien> classList = new List<SinhVien>();
            foreach (DataRow i in DataProvider.Instance.Get_Data(query).Rows)
            {
                classList.Add(GetObjectSinhVien(i));
            }
            return classList;
        }
        public List<SinhVien> GetSinhVien_ByClass_DAL(int ClassID)
        {
            string query = "SELECT * FROM SinhVien WHERE ClassID=" + ClassID;
            List<SinhVien> classList = new List<SinhVien>();
            foreach (DataRow i in DataProvider.Instance.Get_Data(query).Rows)
            {
                classList.Add(GetObjectSinhVien(i));
            }
            return classList;
        }
        public List<SinhVien> GetSinhVien_ByID_DAL(string StudentID)
        {
            string query = "SELECT * FROM SinhVien WHERE StudentID='" + StudentID + "'";
            List<SinhVien> classList = new List<SinhVien>();
            foreach (DataRow i in DataProvider.Instance.Get_Data(query).Rows)
            {
                classList.Add(GetObjectSinhVien(i));
            }
            return classList;
        }
        public List<SinhVien> GetSinhVien_ByName_DAL(string Name)
        {
            string query = "SELECT * FROM SinhVien WHERE Name LIKE '" + Name + "%'";
            List<SinhVien> classList = new List<SinhVien>();
            foreach (DataRow i in DataProvider.Instance.Get_Data(query).Rows)
            {
                classList.Add(GetObjectSinhVien(i));
            }
            return classList;
        }
        public bool AddSinhVien_DAL(SinhVien sv)
        {
            string query = "INSERT INTO SinhVien(StudentID,Name,ClassID,Birthday,Gender) VALUES " +
             "('" +
                 sv.StudentID + "','" +
                 sv.Name + "','" +
                 sv.ClassID + "','" +
                 sv.Birthday + "','" +
                 sv.Gender +
             "')";
            try
            {
                return DataProvider.Instance.Change_Data(query);
            }
            catch(Exception)
            {
                return false;
            }
        }
        public bool EditSinhVien_DAL(SinhVien sv)
        {
            int gender = 1;
            if(sv.Gender)
            {
                gender = 1;
            }
            else
            {
                gender = 0;
            }
            string query = "UPDATE SinhVien SET Name='"
                + sv.Name + "',classID='"
                + sv.ClassID + "',Birthday='"
                + sv.Birthday + "',Gender="
                + gender + " WHERE StudentID='"
                + sv.StudentID + "'";
            try
            {
                return DataProvider.Instance.Change_Data(query);
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool DeleteSinhVien_ByID_DAL(string StudentID)
        {
            return DataProvider.Instance.Change_Data("DELETE FROM SinhVien WHERE StudentID='" + StudentID + "'");
        }
    }
}
