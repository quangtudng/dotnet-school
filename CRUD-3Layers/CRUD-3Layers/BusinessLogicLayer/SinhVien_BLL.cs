﻿using CRUD_3Layers.DataAccessLayer;
using CRUD_3Layers.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUD_3Layers.BusinessLogicLayer
{
    public class SinhVien_BLL
    {
        private static SinhVien_BLL _Instance;

        public static SinhVien_BLL Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new SinhVien_BLL();
                }
                return _Instance;
            }

        }
        private SinhVien_BLL()
        {

        }
        public List<SinhVien> GetAllSinhVien_BLL()
        {
            return SinhVien_DAL.Instance.GetAllSinhVien_DAL();
        }
        public List<SinhVien> GetSinhVien_ByClass_BLL(int ClassID)
        {
            return SinhVien_DAL.Instance.GetSinhVien_ByClass_DAL(ClassID);
        }
        // Vì StudentID là thuộc tính khóa chính nên luôn chỉ có một Sinh viên
        public SinhVien GetSinhVien_ByID_BLL(string StudentID)
        {
            return SinhVien_DAL.Instance.GetSinhVien_ByID_DAL(StudentID)[0];   
        }
        public List<SinhVien> GetSinhVien_ByName_BLL(string Name)
        {
            return SinhVien_DAL.Instance.GetSinhVien_ByName_DAL(Name);
        }
        public bool AddSinhVien_BLL(string StudentID, string Name, int ClassID, DateTime Birthday, bool Gender)
        {
            return SinhVien_DAL.Instance.AddSinhVien_DAL(new SinhVien
            {
                StudentID = StudentID,
                Name = Name,
                ClassID = ClassID,
                Birthday = Birthday,
                Gender = Gender
            });
        }
        public bool EditSinhVien_BLL(string StudentID, string Name, int ClassID, DateTime Birthday, bool Gender)
        {
            return SinhVien_DAL.Instance.EditSinhVien_DAL(new SinhVien
            {
                StudentID = StudentID,
                Name = Name,
                ClassID = ClassID,
                Birthday = Birthday,
                Gender = Gender
            });
        }
        public bool DeleteSinhVien_ByID_BLL(string StudentID)
        {
            return SinhVien_DAL.Instance.DeleteSinhVien_ByID_DAL(StudentID);
        }
        public List<SinhVien> SortSinhVien(List<SinhVien> listSV ,string sortBy)
        {
            if(sortBy == "MSSV")
            {
                listSV = listSV.OrderBy(o => o.StudentID).ToList();
            }
            if(sortBy == "Name")
            {
                listSV = listSV.OrderBy(o => o.Name).ToList();
            }
            if (sortBy == "Class")
            {
                listSV = listSV.OrderBy(o => o.ClassID).ToList();
            }
            if (sortBy == "Birthday")
            {
                listSV = listSV.OrderBy(o => o.Birthday).ToList();
            }
            if (sortBy == "Gender")
            {
                listSV = listSV.OrderBy(o => o.Gender).ToList();
            }
            return listSV;
        }
    }
}