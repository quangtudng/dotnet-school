﻿using CRUD_3Layers.DataAccessLayer;
using CRUD_3Layers.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_3Layers.BusinessLogicLayer
{
    public class Class_BLL
    {
        private static Class_BLL _Instance;

        public static Class_BLL Instance
        { 
            get 
            {
                if(_Instance == null)
                {
                    _Instance = new Class_BLL();
                }
                return _Instance;
            } 
            private set => _Instance = value; 
        }
        private Class_BLL()
        {
        }
        public List<Class> GetAllClass_BLL()
        {
            return Class_DAL.Instance.GetAllClass_DAL();
        }
    }
}
