﻿using CRUD_3Layers.BusinessLogicLayer;
using CRUD_3Layers.DataTransferObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUD_3Layers
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            InitComboBox();
        }
        public void InitComboBox()
        {
            classComboBox2.Items.Add("All");
            foreach (Class cl in Class_BLL.Instance.GetAllClass_BLL())
            {
                classComboBox1.Items.Add(cl);
                classComboBox2.Items.Add(cl);
            }
        }

        private void classComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(classComboBox2.SelectedItem.ToString() == "All")
            {
                dataGridView.DataSource = SinhVien_BLL.Instance.GetAllSinhVien_BLL();
            }
            else
            {
                dataGridView.DataSource = SinhVien_BLL.Instance.GetSinhVien_ByClass_BLL(((Class)classComboBox2.SelectedItem).ClassID);
            }
        }

        private void dataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if(dataGridView.SelectedRows.Count == 1)
            {
                string studentID = dataGridView.SelectedRows[0].Cells["StudentID"].Value.ToString();
                SinhVien sv = SinhVien_BLL.Instance.GetSinhVien_ByID_BLL(studentID);
                IDTextBox.Text = sv.StudentID;
                nameTextBox.Text = sv.Name;
                for(int i = 0;i < classComboBox1.Items.Count; i++)
                {
                    if(((Class)classComboBox1.Items[i]).ClassID == sv.ClassID)
                    {
                        classComboBox1.SelectedIndex = i;
                    }
                }
                birthdayPicker.Value = sv.Birthday;
                if(sv.Gender)
                {
                    maleButton.Checked = true;
                }
                else
                {
                    femaleButton.Checked = true;
                }
            }
        }
        private void addButton_Click(object sender, EventArgs e)
        {
            string StudentID = IDTextBox.Text;
            string Name = nameTextBox.Text;
            int ClassID = ((Class)classComboBox1.SelectedItem).ClassID;
            DateTime Birthday = Convert.ToDateTime(birthdayPicker.Value.Date.ToShortDateString());
            bool Gender = true;
            if(maleButton.Checked)
            {
                Gender = true;
            }
            else if(femaleButton.Checked)
            {
                Gender = false;
            }
            if (!SinhVien_BLL.Instance.AddSinhVien_BLL(StudentID, Name, ClassID, Birthday, Gender))
            {
                MessageBox.Show("An error has occurred during adding user. Please try again");
            }
            classComboBox2.SelectedIndex = 0;
            dataGridView.DataSource = SinhVien_BLL.Instance.GetAllSinhVien_BLL();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            string StudentID = IDTextBox.Text;
            string Name = nameTextBox.Text;
            int ClassID = ((Class)classComboBox1.SelectedItem).ClassID;
            DateTime Birthday = Convert.ToDateTime(birthdayPicker.Value.Date.ToShortDateString());
            bool Gender = true;
            if (maleButton.Checked)
            {
                Gender = true;
            }
            else if (femaleButton.Checked)
            {
                Gender = false;
            }
            if (!SinhVien_BLL.Instance.EditSinhVien_BLL(StudentID, Name, ClassID, Birthday, Gender))
            {
                MessageBox.Show("An error has occurred during editing user");
            }
            classComboBox2.SelectedIndex = 0;
            dataGridView.DataSource = SinhVien_BLL.Instance.GetAllSinhVien_BLL();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if(!SinhVien_BLL.Instance.DeleteSinhVien_ByID_BLL(IDTextBox.Text))
            {
                MessageBox.Show("An error has occured during deleting user");
            }
            classComboBox2.SelectedIndex = 0;
            dataGridView.DataSource = SinhVien_BLL.Instance.GetAllSinhVien_BLL();
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            if(searchTextBox.Text.Trim() == "")
            {
                classComboBox2.SelectedIndex = 0;
                dataGridView.DataSource = SinhVien_BLL.Instance.GetAllSinhVien_BLL();
            }
            else
            {
                dataGridView.DataSource = SinhVien_BLL.Instance.GetSinhVien_ByName_BLL(searchTextBox.Text);
            }
        }

        private void sortButton_Click(object sender, EventArgs e)
        {
           dataGridView.DataSource = SinhVien_BLL.Instance.SortSinhVien((List<SinhVien>)dataGridView.DataSource,sortByComboBox.SelectedItem.ToString());
        }
    }
}
