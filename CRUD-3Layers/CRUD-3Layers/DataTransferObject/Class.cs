﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_3Layers.DataTransferObject
{
    public class Class
    {
        private int _ClassID;
        private string _Name;
        public int ClassID { get => _ClassID; set => _ClassID = value; }
        public string Name { get => _Name; set => _Name = value; }
        public override string ToString()
        {
            return Name;
        }
    }
}
