﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var db = new LibraryManagementEntities();
            // Cách 1 : dùng LINQ câu lệnh query : Với mỗi đỗi tượng p trong list sinh viên ( như vòng lặp foreach) lấy nguyên đối tượng về
            var l1 = from p in db.Books select p;
            // Cách 2 : dùng LINQ bằng method
            var l2 = db.Books.Select(p => p);
            dataGridView1.DataSource = l1.ToList();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // var là kiểu dữ liệu động ,vì ví dụ nếu như lấy thẳng tên đối tượng, khi sử dụng đối tượng chỉ select một vài đối tượng, ta sẽ không cần phải tạo đối tượng mới
            LibraryManagementEntities db = new LibraryManagementEntities();
            // Cách 1 : dùng LINQ câu lệnh query : Với mỗi đỗi tượng p trong list sinh viên ( như vòng lặp foreach) lấy nguyên đối tượng về
            var l1 = from p in db.Books select new { p.BookName, p.Category.CategoryName };
            // Cách 2 : dùng LINQ bằng method
            var l2 = db.Books.Select(p => new { p.BookName, p.Category.CategoryName });
            dataGridView1.DataSource = l2.ToList();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // var là kiểu dữ liệu động ,vì ví dụ nếu như lấy thẳng tên đối tượng, khi sử dụng đối tượng chỉ select một vài đối tượng, ta sẽ không cần phải tạo đối tượng mới
            LibraryManagementEntities db = new LibraryManagementEntities();
            // Cách 1 : dùng LINQ câu lệnh query : Với mỗi đỗi tượng p trong list sinh viên ( như vòng lặp foreach) lấy nguyên đối tượng về
            var l1 = from p in db.Books where p.Category.CategoryID == 1 select new { p.BookName, p.Category.CategoryName };
            // Cách 2 : dùng LINQ bằng method
            var l2 = db.Books.Where(p => p.Category.CategoryID == 1).Select(p => new { p.BookName, p.Category.CategoryName });
            dataGridView1.DataSource = l2.ToList();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // Scalar property  : Thuộc tính cơ bản
            // Navigation property  : Thuộc tính quan hệ
            Book sv = new Book
            {
                BookID="BK1002",
                BookName="Day of deliverance",
                CategoryID=1,
                DateOfReceived= Convert.ToDateTime("2020-01-01"),
                LicenseChecked= true,
            };
            // var là kiểu dữ liệu động ,vì ví dụ nếu như lấy thẳng tên đối tượng, khi sử dụng đối tượng chỉ select một vài đối tượng, ta sẽ không cần phải tạo đối tượng mới
            LibraryManagementEntities db = new LibraryManagementEntities();
            db.Books.Add(sv);
            db.SaveChanges();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            List<string> l = new List<string>();
            // var là kiểu dữ liệu động ,vì ví dụ nếu như lấy thẳng tên đối tượng, khi sử dụng đối tượng chỉ select một vài đối tượng, ta sẽ không cần phải tạo đối tượng mới
            LibraryManagementEntities db = new LibraryManagementEntities();
            foreach(string i in l)
            { 
                Book del = db.Books.Where(p => p.BookID == i).FirstOrDefault();
                db.Books.Remove(del);
                db.SaveChanges();
            }
            db.SaveChanges();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            List<string> l = new List<string>();
            // var là kiểu dữ liệu động ,vì ví dụ nếu như lấy thẳng tên đối tượng, khi sử dụng đối tượng chỉ select một vài đối tượng, ta sẽ không cần phải tạo đối tượng mới
            LibraryManagementEntities db = new LibraryManagementEntities();
            foreach (string i in l)
            {
                Book del = db.Books.Where(p => p.BookID == i).FirstOrDefault();
                del.BookName = "Fuck";
                db.SaveChanges();
            }
            db.SaveChanges();
        }
    }
}
