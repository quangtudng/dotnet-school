﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2.DTO
{
    [Table("LSV")]
    public class LopSV
    {
        [Key][Required]
        public int ID_Lop { get; set; } // Lưu ý, với code first nếu khai báo khoá chính là int nó sẽ tự gắn vào Identity ( Tự động tăng )
        [StringLength(50)][Required]
        public string Ten_Lop { get; set; }

        public virtual ICollection<SV> SVQH { get; set; } // Thuộc tính quan hệ, trả về một danh sách các đối tượng sinh viên
        // Đối tượng lớp sinh viên phải luôn được khởi tạo trước sinh viên
        public LopSV()
        {
            // Để đảm bảo thằng thuộc tính quan hệ này luôn tồn tại
            this.SVQH = new HashSet<SV>();
        }
    }
}
