﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2.DTO
{
    [Table("SinhVien")] // Mặc định sẽ lấy tên class làm tên table nhưng ta có thể làm như vậy để thay đổi, chỉ tác dụng với một thuộc tính ngay sau khai báo
    public class SV
    {
        [Key] // Thông báo trình biên dịch biết đây là khoá chính, tác dụng với một thuộc tính ngay sau khai báo
        public string MSSV { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public int ID_Lop { get; set; }
        [ForeignKey("ID_Lop")] 
        // Để trình biên dịch biết là thuộc tính quan hệ này thiết lập thông qua khoá ngoại
        public virtual LopSV LopSVQH { get; set; } // Thuộc tính quan hệ

    }
}
//2 Cách tạo quan hệ 
//1 là viết trực tiếp ( đơn giản )
//2 dùng On Model Builder ( phức tạp )